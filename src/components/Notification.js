import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Easing, Animated} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import propTypes from 'prop-types'
import colors from '../styles/colors';

export default class Notification extends Component {
  constructor (props) {
    super(props);
    this.state = {
      positionValue: new Animated.Value(60)
    };
    this.closeNotification = this.closeNotification.bind(this);
    
  }

  animatedNotification (value) {
    const { positionValue } = this.state;
    Animated.timing (
      positionValue,
      {
        toValue: value,
        duration: 400,
        velocity: 3,
        tension: 2,
        friction: 8,
        easing: Easing.easeOutBack,
      }
    ).start();
  }

  closeNotification () {
    this.props.handleCloseNotification();
  }

  render () {
    const { type, message, secondLine, showNotification} = this.props;
    const { positionValue } =  this.state
    showNotification ? this.animatedNotification(0) : this.animatedNotification(60);
    return (
      <Animated.View style={[ {transform: [{translateY: positionValue}]}, styles.container]}>
        <View style={styles.notification}>
          <Text style={styles.errorText}>{type}</Text>
          <Text style={styles.errorMessage}>{message}</Text>
          <Text style={styles.errorMessage}>{secondLine}</Text>
        </View>
        <TouchableOpacity style={styles.closeButton} onPress={this.closeNotification}>
          <Icon name="times" size={20} color={colors.lighGrey} />
        </TouchableOpacity>
      </Animated.View>
    );
  }
}

Notification.propTypes = {
  showNotification: propTypes.bool.isRequired,
  type: propTypes.string.isRequired,
  message: propTypes.string,
  secondLine: propTypes.string,
  handleCloseNotification: propTypes.func
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    height: 70,
    width: '100%',
    padding: 10
  },
  notification: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start'
  },
  errorText: {
    color: colors.errorText,
    marginRight: 5,
    fontSize: 14,
    marginBottom: 2
  },
   errorMessage: {
    fontSize: 14,
    marginBottom: 2,
    marginRight: 5
   },
   closeButton: {
    position: 'absolute',
    right: 10,
    top: 10
   }
})