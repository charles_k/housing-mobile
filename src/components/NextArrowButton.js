import React, { Component } from 'react';
import { View, ScrollView, StyleSheet, TouchableHighlight} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import propTypes from 'prop-types'
import colors from '../styles/colors';

export default class NextArrowButton extends Component {
  render () {
    const {handleOnPress, disabled} = this.props;
    const disableButton = disabled ? 0.2 : 0.8
    return (
      <TouchableHighlight style={[{opacity:disableButton}, styles.nextButton]} 
        onPress={handleOnPress}
        disabled={disabled}
        >
        <Icon 
          name="angle-right"
          color={colors.green}
          size={30}
          style={styles.icon}
        />
      </TouchableHighlight>
    )
  }
}

NextArrowButton.propTypes = {
  handleOnPress: propTypes.func.isRequired,
  disabled: propTypes.bool
}

const styles = StyleSheet.create({
  nextButton: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 50,
    width: 50,
    height: 50,
    backgroundColor: colors.white
  },
  icon: {
    
  }
})