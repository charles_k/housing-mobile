import React, { Component } from 'react';
import { View, Image, StyleSheet, Modal, Text} from 'react-native';
import propTypes from 'prop-types'
import colors from '../styles/colors';

export default class Loader extends Component {
  render() {
    const { animationType, visible } = this.props
    return (
      <Modal animationType={animationType} visible={visible} transparent={true}>
        <View style={styles.container}>
            <Image source={require('../assets/images/loader.gif')} style={styles.loaderImage} />
        </View>
      </Modal>
    )
  }
}

Loader.propTypes = {
  animationType: propTypes.string,
  visible: propTypes.bool.isRequired
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    zIndex: 9,
    width: 90,
    height: 90,
    left: '50%',
    top: '50%',
    marginLeft: -45,
    marginTop: -45,
    borderRadius: 15,
  },
  loaderImage: {
    width: 90,
    height: 90,
    borderRadius: 15
  }
})