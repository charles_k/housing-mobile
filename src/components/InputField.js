import React, { Component } from 'react';
import { Text, View, TextInput, StyleSheet, TouchableOpacity, Easing, Animated} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import PropTypes from 'prop-types'
import colors from '../styles/colors';

export default class InputField extends Component {
  constructor (props) {
    super(props);
    this.state = {
      secureInput: props.inputType === 'text' || props.inputType == 'email' ? false : true,
      scaleCheckIconValue: new Animated.Value(0)
    };
    this.toggleShowPassword = this.toggleShowPassword.bind(this)
  }

  scaleCheckIcon (value) {
    Animated.timing (
      this.state.scaleCheckIconValue,
      {
        toValue: value,
        duration: 400,
        easing: Easing.easeOutBack
      }
    ).start();
  }

  toggleShowPassword () {
    this.setState({
      secureInput: !this.state.secureInput
    })
  }

  render () {
    const { labelText, fontSize, color, textColor, inputType, onChangeText, showCheckIcon } = this.props;
    const keyboardType = inputType === 'email' ? 'email-address' : 'default';
    const {secureInput, scaleCheckIconValue} = this.state;

    const iconScale = scaleCheckIconValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0, 1.6, 1]
    });
    const scaleValue = showCheckIcon ? 1 : 0;
    this.scaleCheckIcon(scaleValue);

    return (
      <View style={styles.container}>
        <Text style={[styles.labelText, {color, fontSize}]}> { labelText }</Text>
        { inputType == 'password' ? 
          <TouchableOpacity style={styles.showButton} onPress={this.toggleShowPassword}>
            <Text style={styles.showText}>{secureInput ? 'Show' : 'Hide'}</Text>
          </TouchableOpacity>
          : null
        }
        <Animated.View style={[{transform: [{scale: iconScale}]}, styles.checkIconWrapper]}>
          <Icon name="check-circle" color={colors.white} size={20} />
        </Animated.View>
        <TextInput 
          autoCorrect={false}
          style={[{color:textColor}, styles.inputfield]}
          secureTextEntry={secureInput}
          onChangeText={onChangeText}
          keyboardType={keyboardType}
        />
      </View>
    );
  }
}

InputField.propTypes = {
  labelText: PropTypes.string.isRequired,
  fontSize: PropTypes.number,
  color: PropTypes.string,
  textColor: PropTypes.string,
  inputType: PropTypes.string.isRequired,
  onChangeText: PropTypes.func,
  showCheckIcon: PropTypes.bool.isRequired
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    marginBottom: 30
  },
  labelText: {
    fontWeight: '700',
    marginBottom: 15,
  },
  inputfield: {
    borderBottomWidth: 1,
    paddingTop: 5,
    paddingBottom: 5,
    borderBottomColor: colors.white
  },
  showButton: {
    position: 'absolute',
    right: 0
  },
  showText: {
    color: colors.white,
    fontWeight: '500'
  },
  checkIconWrapper: {
    position: 'absolute',
    right: 0,
    bottom: 10
  }
})