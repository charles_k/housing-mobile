import React, { Component } from 'react';
import { Text, View, TouchableHighlight, StyleSheet} from 'react-native';
import propTypes from 'prop-types';
import colors from '../styles/colors';

export default class Button extends Component {
  render () {
    const { text, color, backgroundColor, icon, handleOnPress} = this.props;
    return (
      <TouchableHighlight 
        style={[ styles.ButtonWrapper, {backgroundColor}]}
        onPress={handleOnPress}
        >
        <View style={styles.buttonTextWrapper}>
          {icon}
          <Text style={[styles.buttonText, {color}]}>
            {text}
          </Text>
        </View>
      </TouchableHighlight>
    );
  }
}

Button.propTypes = {
  text: propTypes.string.isRequired,
  color: propTypes.string,
  backgroundColor: propTypes.string,
  icon: propTypes.object,
  handleOnPress: propTypes.func.isRequired
};

const styles = StyleSheet.create({
  ButtonWrapper: {
    display: 'flex',
    padding: 15,
    borderRadius: 40,
    borderWidth: 1,
    borderColor: colors.white,
    marginBottom: 15,
    alignItems: 'center'
  },
  buttonTextWrapper: {
    flexDirection: 'row',
    justifyContent: 'flex-end'
  },
  buttonText: {
    fontSize: 18,
    width: '100%',
    textAlign: 'center'
  }
})