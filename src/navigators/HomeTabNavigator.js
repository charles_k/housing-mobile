import {TabNavigator} from 'react-navigation'
import Explore from '../containers/Explore';
import Inbox from '../containers/inbox';
import Profile from '../containers/Profile';
import Saved from '../containers/Saved';
import Trips from '../containers/Trips';

const HomeTabNavigator = TabNavigator({
  Explore: {screen: Explore} ,
  Inbox: {screen: Inbox},
  Profile: {screen: Profile},
  Saved: {screen: Saved},
  Trips: {screen: Trips},
}, {
  tabBarOptions: {
    fontWeight: '600',
  }
});

export default HomeTabNavigator;