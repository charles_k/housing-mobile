import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, KeyboardAvoidingView} from 'react-native';
import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import {ActionCreators} from '../redux/actions'
import InputField from '../components/InputField'
import colors from '../styles/colors';
import NextArrowButton from '../components/NextArrowButton'; 
import Notification from '../components/Notification';
import Loader from '../components/Loader';

class LogIn extends Component {
  constructor (props) {
    super(props);
    this.state = {
      formValid: true,
      validEmail: false,
      validPassword: false,
      emailAddress: '',
      password: '',
      loading: false
    }
    this.handleCloseNotification = this.handleCloseNotification.bind(this);
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.NextButtonHandler = this.NextButtonHandler.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.toggleNextButtonState = this.toggleNextButtonState.bind(this);
  }
  NextButtonHandler () {
    this.setState({loading: true})

    setTimeout( () => {
      const {emailAddress, password} = this.state;

     if (this.props.logIn(emailAddress, password)) {
      this.setState({formValid: true, loading: false})
     } else {
      this.setState({formValid: false, loading: false})
     }
    }, 2000);
  }
  handleCloseNotification () {
    this.setState( { formValid: true })
  }
  handleEmailChange (email) {
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.setState({ emailAddress: email });

    if (!this.state.validEmail) {
      if (emailRegex.test(email)) {
        this.setState({ validEmail: true });
      }
    } else {
      if (!emailRegex.test(email)) {
        this.setState({ validEmail: false })
      }
    }
  }

  handlePasswordChange (password) {
    this.setState({password})

    if (!this.state.validPassword) {
      if (password.length > 4) {
        this.setState({ validPassword: true});
      }
    } else if (password <= 4) {
      this.setState({ validPassword: false});
    }
  }

  toggleNextButtonState () {
    const {validEmail, validPassword} = this.state;
    if (validEmail && validPassword) {
      return false;
    }
    return true;
  }

  render () {
    const {formValid, loading, validEmail, validPassword} =  this.state
    const showNotification = formValid ? false : true
    const background = formValid ? colors.green : colors.errorBackground
    return (
      <KeyboardAvoidingView style={[{backgroundColor: background}, styles.container]} behavior="padding">
        <View style={styles.wrapper}>
          <ScrollView style={styles.scrollview}>
            <Text style={styles.logInText}>Log In</Text>
            <InputField labelText="EMAIL ADDRESS" fontSize={14}
              color={colors.white}
              textColor={colors.white}
              inputType="email"
              onChangeText={this.handleEmailChange}
              showCheckIcon={validEmail}
            />
            <InputField labelText="PASSWORD" fontSize={14}
              color={colors.white}
              textColor={colors.white}
              inputType="password"
              onChangeText={this.handlePasswordChange}
              showCheckIcon={validPassword}
            />
          </ScrollView>
          <View style={styles.NextArrowButton}>
            <NextArrowButton 
              handleOnPress={this.NextButtonHandler}
              disabled={this.toggleNextButtonState()}
            />
          </View>
          <View>
            <Notification
              showNotification={showNotification}
              type="Error:"
              message="These credentials doesn't look right"
              secondLine="Please try again"
              handleCloseNotification={this.handleCloseNotification}
            />
          </View>
        </View>
        <Loader
          visible={loading}
          animationType="fade"
        />
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    marginTop: 70,
    flex: 1
  },
  container: {
    display: 'flex',
    flex: 1
  },
  scrollview: {
    padding: 30,
    flex: 1
  },
  logInText: {
    fontSize: 28,
    color: colors.white,
    fontWeight: '600',
    marginBottom: 30
  },
  NextArrowButton: {
    alignItems: 'flex-end',
    bottom: 20,
    right: 20
  }
})

const mapStateToProps = (state) => {
  return { loggedInStatus: state.loggedInStatus, }
};

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators(ActionCreators, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(LogIn);