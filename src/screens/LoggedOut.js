import React, { Component } from 'react';
import { Text, View, Image, StyleSheet, TouchableHighlight } from 'react-native';
import colors from '../styles/colors';
import Button from '../components/Button';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class LoggedOut extends Component {

  onFacebookButtonPress () {
    alert('button pRESSED')
  }

  onAccountButtonPress () {
    alert('button pressed 2')
  }

  onMoreButtonPress () {
    alert('More button pressed')
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Image style={styles.logo}
            source={require('../assets/images/logo.png')}
          />
          <Text style={styles.welcomeText}>Welcome to AirBnB</Text>
          <Button 
            text="Continue with Facebook"
            color={colors.green}
            backgroundColor={colors.white}
            icon={ <Icon name="facebook" size={20} style={styles.facebookIcon}/> }
            handleOnPress={this.onFacebookButtonPress}
          />
          <Button 
            text="Create an Account"
            color={colors.white}
            handleOnPress={this.onAccountButtonPress}
          />
          <TouchableHighlight style={styles.moreButton} onPress={this.onMoreButtonPress}>
            <Text style={styles.moreText}>More Options</Text>
          </TouchableHighlight>

          <View style={styles.termsWrapper}>
            <Text style={styles.termsText}>By tapping Continue, Create Account or More</Text>
            <Text style={styles.termsText}>options, I agree to Airbnd's</Text>
            <TouchableHighlight style={styles.linkButton}>
              <Text style={styles.termsText}>Terms of Service, </Text>
            </TouchableHighlight>
            <TouchableHighlight style={styles.linkButton}>
              <Text style={styles.termsText}>Payments Terms of Service, </Text>
            </TouchableHighlight>
            <TouchableHighlight style={styles.linkButton}>
              <Text style={styles.termsText}>Privacy Policy, </Text>
            </TouchableHighlight>
            <Text style={styles.termsText}>and </Text>
            <TouchableHighlight style={styles.linkButton}>
              <Text style={styles.termsText}>Nondiscrimination Policy. </Text>
            </TouchableHighlight>
          </View>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create ({
  container: {
    flex: 1,
    display: 'flex',
    backgroundColor: colors.green
  },
  wrapper: {
    flex: 1,
    display: 'flex',
    marginTop: 30,
    padding: 20
  },
  logo: {
    width: 50,
    height: 50,
    marginTop: 50,
    marginBottom: 40
  },
  welcomeText: {
    fontSize: 30,
    color: colors.white,
    fontWeight: '300',
    marginBottom: 40
  },
  facebookIcon: {
    color: colors.green,
    fontWeight: 'bold',
    position: 'relative',
    left: 20,
    zIndex: 8
  },
  moreButton: {
    marginTop: 15,
  },
  moreText: {
    color: colors.white,
    fontSize: 16,
  },
  termsWrapper: {
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    flexDirection: 'row',
    marginTop: 30
  },
  termsText: {
    color: colors.white,
    fontSize: 15,
    fontWeight: '400'
  },
  linkButton: {
    borderBottomWidth: 1,
    borderBottomColor: colors.white
  }
})