import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, KeyboardAvoidingView} from 'react-native';
import colors from '../styles/colors';
import InputField from '../components/InputField';
import NextArrowButton from '../components/NextArrowButton';
import Loader from '../components/Loader';
import Notification from '../components/Notification';

export default class ForgetPassword extends Component {
  constructor (props) {
    super(props);
    this.state = {
      validEmail: false,
      emailAddress: '',
      formValid: true,
      loading: false
    }
    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.goToNextStep = this.goToNextStep.bind(this);
    this.handleCloseNotification = this.handleCloseNotification.bind(this);
  }

  handleEmailChange (email) {
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    this.setState({ emailAddress: email });

    if (!this.state.validEmail) {
      if (emailRegex.test(email)) {
        this.setState({ validEmail: true });
      }
    } else {
      if (!emailRegex.test(email)) {
        this.setState({ validEmail: false })
      }
    }
  }

  handleCloseNotification () {
    this.setState( { formValid: true })
  }

  toggleNextButtonState () {
    const {validEmail} = this.state;
    if (validEmail) {
      return false;
    }
    return true;
  }

  goToNextStep () {
    this.setState({loading: true})
    setTimeout( () => {
      if (this.state.emailAddress == 'hello@charles.com') {
        this.setState({formValid: true, loading: false})
        alert('success')
      } else {
        this.setState({formValid: false, loading: false})
      }
    }, 2000);
  }

  render () {
    const {loading, validEmail, formValid} = this.state;
    const background = formValid ? colors.green : colors.errorBackground
    const showNotification = formValid ? false : true
    return (
      <KeyboardAvoidingView behavior="padding" style={[styles.container, {backgroundColor: background}]}>
        <View style={styles.wrapper}>
          <Text style={styles.forgetPassText}>Forgot your password</Text>
          <Text style={styles.subText}>Enter your email to find your account</Text>
          <InputField labelText="EMAIL ADDRESS" fontSize={14}
            color={colors.white}
            textColor={colors.white}
            inputType="email"
            onChangeText={this.handleEmailChange}
            showCheckIcon={validEmail}
          />
         </View>
         <View style={styles.NextArrowButton}>
           <NextArrowButton 
            handleOnPress={this.goToNextStep}
            disabled={this.toggleNextButtonState()}
           />
         </View>
         <View>
            <Notification
              showNotification={showNotification}
              type="Error:"
              message="These credentials doesn't look right"
              secondLine="Please try again"
              handleCloseNotification={this.handleCloseNotification}
            />
          </View>
         <Loader 
          visible={loading}
          animationType="fade"
         />
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flex: 1,
  },
  wrapper: {
    marginTop: 90,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1
  },
  forgetPassText: {
    fontSize: 28,
    color: colors.white,
    fontWeight: '300'
  },
  subText: {
    color: colors.white,
    fontWeight: '600',
    fontSize: 15,
    marginTop: 10,
    marginBottom: 60
  },
  NextArrowButton: {
    alignItems: 'flex-end',
    bottom: 20,
    right: 20
  }
})