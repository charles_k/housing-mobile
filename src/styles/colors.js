export default {
  tabIconDefault: '#ccc',
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: 'red',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeText: '#fff',
  lighGrey: '#d8d8d8',
  green: '#008388',
  white: '#fff'
};
