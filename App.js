import React, {Component} from 'react';
import {Provider} from 'react-redux'
import store from './src/redux/store'
// import ForgetPassword from './src/screens/ForgetPassword';
// import LoggedOut from './src/screens/LoggedOut';
import LogIn from './src/screens/LogIn';

export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <LogIn />
      </Provider>
    );
  }
}



